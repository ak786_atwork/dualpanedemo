package com.example.dualpaneappdemo

interface MyCommunicator { // Meant for inter-fragment communication

    fun displayDetails(title: String, description: String)
}